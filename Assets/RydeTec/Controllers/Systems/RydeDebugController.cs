using UnityEngine;
using System.Collections;

public class RydeDebug {
    public static Queue queue = new Queue();
    public static int maxQueue = 16;

    static RydeDebug() {
        
    }

    public static void Send(string message) {
        queue.Enqueue(message);
    }

    public static string Read() {
        if (queue.Count > 0) {
            return (string) queue.Dequeue();
        }
        return null;
    }
}

public class RydeDebugController : MonoBehaviour
{
    string[] texts;
    TextMesh textMesh;

	// Use this for initialization
	void Start()
	{
        texts = new string[RydeDebug.maxQueue];
        textMesh = gameObject.transform.GetComponentInChildren<TextMesh>();
        textMesh.text = "";

        RydeDebug.Send("RydeDebugController started.");
	}

	// Update is called once per frame
	void Update()
	{
        string line = RydeDebug.Read();
        if (line != null) {
            
            add_to_texts(line);
            texts_to_mesh();
        }
	}

    void add_to_texts(string text) {
        string[] newTexts = new string[RydeDebug.maxQueue];
        newTexts[0] = text;
        int next = 1;
        foreach (string t in texts)
        {
            if (next >= texts.Length)
            {
                break;
            }
            newTexts[next] = t;
            next += 1;
        }
        texts = newTexts;
    }

    void texts_to_mesh() {
        textMesh.text = "";
        foreach (string t in texts) {
            if (t != null) {
                textMesh.text += t + "\n";
            }
        }
    }
}
