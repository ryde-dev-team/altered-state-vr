﻿using UnityEngine;
using System.Collections;


public class WoodcuttingController : MonoBehaviour
{
    public static bool modeActive;
    public static bool shouldHit;
    bool stoppedSwinging;
    float lastHit;

    public static GameObject currentTree;
    // Use this for initialization
    void Start()
    {
        modeActive = false;

        shouldHit = false;
        stoppedSwinging = true;

        lastHit = 0;

        currentTree = null;
    }

    public static void ShouldHit() {
        LocalNotification.Send("You hit the tree");
    }

    public static void ShouldMiss()
    {
        LocalNotification.Send("Missed!");
    }

    // Update is called once per frame
    void Update()
    {
        if (modeActive) {
            Vector3 av = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch);

            if (av[1] > 2.5 && stoppedSwinging && (Time.time - lastHit >= 0.6f))
            {
                OVRDebug.Log("You swung at the tree");
                currentTree.GetComponent<AudioSource>().Play();
                stoppedSwinging = false;

                NetworkSystem.Command command = new NetworkSystem.Command(NetworkSystem.Command.Protocol.alter,
                                                                          new NetworkSystem.Command.Data[] {
                    new NetworkSystem.Command.Data("skill", new NetworkSystem.Command.Data[] {
                        new NetworkSystem.Command.Data("action", new NetworkSystem.Command.Data[] {
                            new NetworkSystem.Command.Data("name", "Woodcutting")
                        })
                    })
                });

                OVRDebug.Log(command);
                NetworkSystem.SendCommand(command);


                //int hitChance = Random.Range(0, 10);

                //if (hitChance > 0)
                //{
                //    shouldHit = true;
                //}
                //else
                //{
                //    LocalNotification.Send("Missed!");
                //}

                lastHit = Time.time;
            }

            if (av[1] < 0.0 && !stoppedSwinging)
            {
                stoppedSwinging = true;
            }

            if (shouldHit)
            {
                LocalNotification.Send("You hit the tree");



                //int lootChance = Random.Range(0, 9);

                //if (lootChance == 0)
                //{

                //    LocalNotification.Send("+7 xp");

                //    RydePlayerController.skills[(int)Skill.Type.woodcutting].gain(7);

                //    if (RydePlayerController.addItemToInventory(new Item("Log", false, 1, 75)) > 0) {
                //        LocalNotification.Send("+1 log");
                //    } else {
                //        LocalNotification.Send("No room!");
                //    }

                //}

                shouldHit = false;
            }
        }
    }
}
