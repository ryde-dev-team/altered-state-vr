﻿using UnityEngine;
using System.Collections;

public class MiningController : MonoBehaviour
{
    public static bool modeActive;
    bool shouldHit;
    bool stoppedSwinging;
    float lastHit;

    public static GameObject currentOre;
    // Use this for initialization
    void Start()
    {
        modeActive = false;

        shouldHit = false;
        stoppedSwinging = true;

        lastHit = 0;

        currentOre = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (modeActive)
        {
            Vector3 av = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch);

            if (av[0] < -2.5 && stoppedSwinging && (Time.time - lastHit >= 0.6))
            {
                OVRDebug.Log("You swung at the ore");
                stoppedSwinging = false;

                int hitChance = Random.Range(0, 10);

                if (hitChance > 0)
                {
                    shouldHit = true;
                }
                else
                {
                    LocalNotification.Send("Missed!");
                }

                lastHit = Time.time;
            }

            if (av[0] > 0.0 && !stoppedSwinging)
            {
                stoppedSwinging = true;
            }

            if (shouldHit)
            {
                LocalNotification.Send("You hit the ore");

                currentOre.GetComponent<AudioSource>().Play();

                int lootChance = Random.Range(0, 9);

                if (lootChance == 0)
                {

                    LocalNotification.Send("+7 xp");

                    RydePlayerController.skills[(int)Skill.Type.mining].gain(7);

                    if (RydePlayerController.addItemToInventory(new Item("Ore", false, 1, 100)) > 0)
                    {
                        LocalNotification.Send("+1 ore");
                    }
                    else
                    {
                        LocalNotification.Send("No room!");
                    }

                }

                shouldHit = false;
            }
        }
    }
}
