using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour
{

    public static string[] characters = new string[4];
    public static string[] servers = new string[1];

    public static TextMesh[] gameButtons = new TextMesh[4];
    public static TextMesh serverName;

    public static bool waitingForServerVerify = false;
    public static string characterWaiting = "";

    public static GameObject loadingTextObject;
    public static TextMesh loadingText;

    public static string[] load_letters = new string[] {"P","l","e","a","s","e"," ","w","a","i","t",".",".","."};
    public static Queue letterQueue = new Queue();

    bool show_slots = false;

	// Use this for initialization
	void Start()
	{
        OVRDebug.Log("Start!");
        if (!NetworkSystem.isStarted)
        {
            //try {
                NetworkSystem.Start();
            //} catch (Exception e) {
                //OVRDebug.Log("Network did not start. " + e);
            //}

        }

        OVRDebug.Log("Buttons!");
        gameButtons[0] = GameObject.Find("character1").transform.Find("name").GetComponent<TextMesh>();
        gameButtons[1] = GameObject.Find("character2").transform.Find("name").GetComponent<TextMesh>();
        gameButtons[2] = GameObject.Find("character3").transform.Find("name").GetComponent<TextMesh>();
        gameButtons[3] = GameObject.Find("character4").transform.Find("name").GetComponent<TextMesh>();

        OVRDebug.Log("Server!");
        serverName = GameObject.Find("server").GetComponent<TextMesh>();

        OVRDebug.Log("Loading Text!");
        loadingTextObject = GameObject.Find("loadingText");
        loadingText = loadingTextObject.GetComponent<TextMesh>();

        OVRDebug.Log("Queing Letters");
        for (int l = 0; l < load_letters.Length; l++){
            letterQueue.Enqueue(load_letters[l]);
        }
        OVRDebug.Log("Starting coroutine?");
        loadingText.text = "";
        StartCoroutine(update_loading_text());


	}

    IEnumerator update_loading_text(){
        OVRDebug.Log("Starting coroutine");
        while(letterQueue.Count > 0) {
            loadingText.text = loadingText.text + letterQueue.Dequeue();
            yield return new WaitForSeconds(.1f);
        }
    }

	// Update is called once per frame
	void Update()
	{
        CommandController.runNextCommand();

        if (gameButtons[0].gameObject.activeInHierarchy) {
            gameButtons[0].text = characters[0];
            gameButtons[1].text = characters[1];
            gameButtons[2].text = characters[2];
            gameButtons[3].text = characters[3];

            serverName.text = servers[0];

            bool slot_available = false;

            for (int i = 0; i < gameButtons.Length; i++)
            {
                if (gameButtons[i].text == "")
                {
                    gameButtons[i].text = "Empty";
                    gameButtons[i].gameObject.SetActive(true);
                    slot_available = true;
                    break;
                }
            }

            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            {
                if (EquippedController.selectedObject.name == "character1")
                {
                    select_slot(0);
                }

                if (EquippedController.selectedObject.name == "character2")
                {
                    select_slot(1);
                }

                if (EquippedController.selectedObject.name == "character3")
                {
                    select_slot(2);
                }

                if (EquippedController.selectedObject.name == "character4")
                {
                    select_slot(3);
                }
            }
        }

        if (characterWaiting != "" && !waitingForServerVerify) {
            // Persistent data between scenes cannot be held in MonoBehaviors that are "restarted" for each load.
            ServerCharacterController.character = new Character(characterWaiting, 0.0f, 0.0f, 0.0f);
            SceneManager.LoadScene("testMotion",LoadSceneMode.Single);
        }
	}

    public void hide_loading_text(){
        loadingTextObject.GetComponent<Animator>().Play("loading_text_animation");
    }

    public void select_slot(int slot){
        OVRDebug.Log("Load character " + slot);

        NetworkSystem.Command command;

        if (gameButtons[slot].text == "Empty")
        {
            command = new NetworkSystem.Command(NetworkSystem.Command.Protocol.alter,
                                                                  new NetworkSystem.Command.Data[] {
                        new NetworkSystem.Command.Data("new",
                                                       new NetworkSystem.Command.Data[] {
                    new NetworkSystem.Command.Data("character", "test" + slot),
                    new NetworkSystem.Command.Data("slot", "" + slot)
                        })
            });

            characters[slot] = "test" + slot;

        }
        else
        {
            command = new NetworkSystem.Command(NetworkSystem.Command.Protocol.login,
                                                                  new NetworkSystem.Command.Data[] {
                        new NetworkSystem.Command.Data("character",
                                                       new NetworkSystem.Command.Data[] {
                            new NetworkSystem.Command.Data("slot", "" + slot)
                        })
            });


        }

        NetworkSystem.SendCommand(command);

        waitingForServerVerify = true;
        characterWaiting = characters[slot];
    }
}
