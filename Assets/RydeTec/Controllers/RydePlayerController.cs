using UnityEngine;
using System.Collections;
using System;

public class RydePlayerController : MonoBehaviour
{

    public static Skill[] skills;

    public static Item[] items;
    public static Item[] bank;
    public static WorldItem[] worldItems;

    public static bool moving;
    public static bool running;
    public static GameObject playerController;

    int notificationDecay;
    TextMesh notification;

    AudioSource footsteps;
    float stepPitch;

    bool lastPace = false;
    bool currentPace = false;

    public static bool readyForServer = false;
    public static bool inSync = false;

	// Use this for initialization
	void Start()
	{

        playerController = GameObject.Find("SamplePlayerController");
        notification = GameObject.Find("NotificationText").GetComponent<TextMesh>();

        skills = new Skill[] { new Skill("Health", 10, 10, 1154),
            new Skill("Attack", 1, 1, 0),
            new Skill("Strength", 1, 1, 0),
            new Skill("Defense", 1, 1, 0),
            new Skill("Archery", 1, 1, 0),
            new Skill("Magic", 1, 1, 0),
            new Skill("Stealth", 1, 1, 0),
            new Skill("Woodcutting", 1, 1, 0),
            new Skill("Fishing", 1, 1, 0),
            new Skill("Mining", 1, 1, 0),
            new Skill("Gathering", 1, 1, 0),
            new Skill("Cooking", 1, 1, 0),
            new Skill("Blacksmithing", 1, 1, 0),
            new Skill("Crafting", 1, 1, 0),
            new Skill("Farming", 1, 1, 0),
            new Skill("Alchemy", 1, 1, 0)
        };

        notificationDecay = 10;

        items = new Item[21];

        bank = new Item[60];

        worldItems = new WorldItem[100];

        moving = false;
        running = false;

        footsteps = GameObject.Find("Footsteps").GetComponent<AudioSource>();
        stepPitch = footsteps.pitch;
        readyForServer = true;
	}

    public static double hasMoney(double val) {
        double totalMoney = 0;

        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] != null)
            {
                if (items[i].name == "Copper")
                {
                    totalMoney += items[i].stack;
                }
                if (items[i].name == "Silver")
                {
                    totalMoney += items[i].stack * 1000;
                }
                if (items[i].name == "Gold")
                {
                    totalMoney += items[i].stack * 1000 * 1000;
                }
                if (items[i].name == "Platinum")
                {
                    totalMoney += items[i].stack * 1000 * 1000 * 1000;
                }
            }
        }

        if (totalMoney >= val) {
            return 0;
        }
        return val - totalMoney;
    }

    public static bool removeMoney(double val) {
        double moneyNeeded = hasMoney(val);

        if (moneyNeeded > 0) {
            LocalNotification.Send("Need " + moneyNeeded + " more copper.");
            return false;
        }

        double moneyLeft = val;
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] != null)
            {
                if (items[i].name == "Copper")
                {
                    int rm = items[i].removeAmount((int) val);
                    moneyLeft -= rm;
                    break;
                }
                if (items[i].name == "Silver")
                {
                    int rm = items[i].removeAmount(1);
                    moneyLeft -= rm * 1000;
                    break;
                }
                if (items[i].name == "Gold")
                {
                    int rm = items[i].removeAmount(1);
                    moneyLeft -= rm * 1000 * 1000;
                    break;
                }
                if (items[i].name == "Platinum")
                {
                    int rm = items[i].removeAmount(1);
                    moneyLeft -= rm * 1000 * 1000 * 1000;
                    break;
                }
            }
        }

        if (moneyLeft < 0)
        {
            RydePlayerController.addItemToInventory(new Item("Copper", true, - (int) moneyLeft));
            return true;
        }

        if (moneyLeft > 0) {
            OVRDebug.Log("Used currency checking next tier");
            return removeMoney(moneyLeft);
        }

        return true;
    }

    public static bool removeItemFromInventory(Item item) {
        //TCPTestClient.Send("Remove " + item.stack + " " + item.name + " from the bank.");
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] != null)
            {
                if (items[i].name == item.name)
                {
                    if (items[i].stack >= item.stack) {
                        items[i].removeAmount(item.stack);
                        return true;
                    } else {
                        OVRDebug.Log("Not enough items to remove.");
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public static bool removeItemFromBank(Item item)
    {
        //TCPTestClient.Send("Remove " + item.stack + " " + item.name + " from the bank.");
        for (int i = 0; i < bank.Length; i++)
        {
            if (bank[i] != null)
            {
                if (bank[i].name == item.name)
                {
                    if (bank[i].stack >= item.stack)
                    {
                        bank[i].removeAmount(item.stack);
                        return true;
                    }
                    else
                    {
                        OVRDebug.Log("Not enough items to remove.");
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public static int addItemToInventory(Item item) {

        //TCPTestClient.Send("Add " + item.stack + " " + item.name + " to the inventory.");
        if (item.stackable)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] != null)
                {
                    if (items[i].name == item.name)
                    {
                        OVRDebug.Log("Item added to stack.");


                        int rm = items[i].addAmount(item.stack);

                        GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                        return rm;
                    }
                }
            }

            int numDeposited = 0;
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == null)
                {
                    OVRDebug.Log("Item added to blank slot.");

                    items[i] = new Item(item.name, item.stackable, 1, item.value);
                    numDeposited += 1;
                    break;
                }
            }

            if ((item.stack - numDeposited) > 0) {
                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i] != null)
                    {
                        if (items[i].name == item.name)
                        {
                            OVRDebug.Log("Item added to stack.");
                            int rm = items[i].addAmount(item.stack - numDeposited);

                            GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                            return rm;
                        }
                    }
                }
            }

            if (numDeposited > 0) {
                GameObject.Find("Stored").GetComponent<AudioSource>().Play();
            }
            return item.stack - (item.stack - numDeposited);
        }

        if (!item.stackable && item.stack > 1) {
            int numDeposited = 0;
            for (int n = 0; n < item.stack; n++){
                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i] == null)
                    {
                        OVRDebug.Log("Item added to blank slot.");
                        items[i] = new Item(item.name, item.stackable, 1, item.value);
                        numDeposited += 1;
                        //break;
                        GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                        return 1;
                    }
                }
            }

            if (numDeposited > 0) {
                GameObject.Find("Stored").GetComponent<AudioSource>().Play();
            }
            return item.stack - (item.stack - numDeposited);
        }

        for (int i = 0; i < items.Length; i++) {
            if (items[i] == null) {
                OVRDebug.Log("Item added to blank slot.");
                items[i] = item;
                GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                return item.stack;
            }
        }
        return 0;
    }

    public static int addItemToBank(Item item)
    {
        //TCPTestClient.Send("Add " + item.stack + " " + item.name + " to the bank.");
        for (int i = 0; i < bank.Length; i++)
        {
            if (bank[i] != null)
            {
                if (bank[i].name == item.name) {
                    OVRDebug.Log("Item added to stack.");
                    int rm = bank[i].addAmount(item.stack);

                    GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                    return rm;
                }
            }
        }

        for (int i = 0; i < bank.Length; i++)
        {
            if (bank[i] == null)
            {
                OVRDebug.Log("Item added to blank slot.");
                bank[i] = item;

                GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                return item.stack;
            }
        }
        return 0;
    }

	// Update is called once per frame
	void Update()
	{
        CommandController.runNextCommand();

        if (!inSync)
        {
            if (NetworkSystem.readyForPlayer)
            {
                playerController.transform.position = new Vector3(ServerCharacterController.character.pos[0],
                                                              ServerCharacterController.character.pos[1],
                                                              ServerCharacterController.character.pos[2]);
                OVRDebug.Log("Setting player position server");
                inSync = true;
            }
            else
            {
                OVRDebug.Log("Server not ready for player");
                return;
            }
        }

        notificationDecay--;
        if (notificationDecay <= 0) {
            notificationDecay = 10;
            LocalNotification.Send("");
        }


        if (LocalNotification.text != "")
        {
            notification.text = LocalNotification.text;
        }

        if (!RydePlayerController.moving && GameObject.Find("Footsteps").GetComponent<AudioSource>().isPlaying) {
            GameObject.Find("Footsteps").GetComponent<AudioSource>().Stop();
        }

        if (RydePlayerController.moving) {
            ServerCharacterController.character.setPosition(playerController.transform.position.x,
                                                       playerController.transform.position.y,
                                                       playerController.transform.position.z);


            //currentPace = false = moving
            //              true  = running

            if (running) {
                currentPace = true;
            } else {
                currentPace = false;
            }

            if (currentPace != lastPace) {
                lastPace = currentPace;
                OVRDebug.Log("Pace changed!");
                footsteps.Pause();
            }

            if (!footsteps.isPlaying)
            {
                if (running)
                {
                    OVRDebug.Log("Setting pitch to run");
                    footsteps.pitch = stepPitch * 1.3f;
                }
                else
                {
                    OVRDebug.Log("Setting pitch to walk");
                    footsteps.pitch = stepPitch;
                }

                OVRDebug.Log("Playing footsteps");
                footsteps.Play();
            }

            //footsteps.pitch = stepPitch;
            //if (running) {
                //footsteps.Stop();
                //footsteps.pitch = stepPitch * 3f;
            //}

            //if (!footsteps.isPlaying){
                //footsteps.Play();
            //}
        } else {
            if (footsteps.isPlaying)
            {
                OVRDebug.Log("Stopping footsteps");
                footsteps.Stop();
            }
        }
	}
}
