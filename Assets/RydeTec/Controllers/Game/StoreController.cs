﻿using UnityEngine;
using System.Collections;

public class StoreController : MonoBehaviour
{
    static GameObject inventoryScreen;
    static GameObject debugLog;
    static GameObject characterScreen;
    static GameObject skillScreen;

    static GameObject bankScreen;

    static GameObject controlObject;
    static GameObject centerEye;

    public static Item[] items;

    public static bool modeActive;

    // Use this for initialization
    void Start()
    {
        controlObject = GameObject.Find("Control");

        centerEye = GameObject.Find("CenterEyeAnchor");

        inventoryScreen = GameObject.Find("InventoryScreen");
        debugLog = GameObject.Find("DebugLog");
        characterScreen = GameObject.Find("CharacterScreen");
        skillScreen = GameObject.Find("SkillScreen");

        bankScreen = GameObject.Find("BankScreen");

        modeActive = false;

        items = new Item[60];

        items[0] = new Item("Log", false, 10, 75);
        items[1] = new Item("Ore", false, 4, 100);
    }

    public static int Sell(Item item) {
        //TCPTestClient.Send("Sold " + item.stack + " " + item.name + " to the store.");
        if (item.stackable)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] != null)
                {
                    if (items[i].name == item.name)
                    {
                        OVRDebug.Log("Item added to stack.");


                        int rm = items[i].addAmount(item.stack);

                        GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                        return rm;
                    }
                }
            }

            int numDeposited = 0;
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == null)
                {
                    OVRDebug.Log("Item added to blank slot.");

                    items[i] = new Item(item.name, item.stackable, 1, item.value);
                    numDeposited += 1;
                    break;
                }
            }

            if ((item.stack - numDeposited) > 0)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i] != null)
                    {
                        if (items[i].name == item.name)
                        {
                            OVRDebug.Log("Item added to stack.");
                            int rm = items[i].addAmount(item.stack - numDeposited);

                            GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                            return rm;
                        }
                    }
                }
            }

            if (numDeposited > 0)
            {
                GameObject.Find("Stored").GetComponent<AudioSource>().Play();
            }
            return (item.stack - (item.stack - numDeposited));
        }

        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] != null)
            {
                if (items[i].name == item.name)
                {
                    OVRDebug.Log("Item added to stack.");
                    int rm = items[i].addAmount(item.stack);

                    GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                    return rm;
                }
            }
        }

        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] == null)
            {
                OVRDebug.Log("Item added to blank slot.");
                items[i] = item;
                GameObject.Find("Stored").GetComponent<AudioSource>().Play();
                return item.stack;
            }
        }
        return 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            if (EquippedController.selectedObject.layer == 21 && EquippedController.selectedObject.name == "store")
            {
                modeActive = true;

                controlObject.transform.rotation = centerEye.transform.rotation;

                if (!inventoryScreen.activeInHierarchy)
                {
                    inventoryScreen.SetActive(true);
                }
                if (!bankScreen.activeInHierarchy)
                {
                    bankScreen.SetActive(true);
                }

                if (debugLog.activeInHierarchy)
                {
                    debugLog.SetActive(false);
                }
                if (characterScreen.activeInHierarchy)
                {
                    characterScreen.SetActive(false);
                }
                if (skillScreen.activeInHierarchy)
                {
                    skillScreen.SetActive(false);
                }


            }
        }
    }
}
