﻿using UnityEngine;
using System.Collections;

public class BankController : MonoBehaviour
{
    static GameObject inventoryScreen;
    static GameObject debugLog;
    static GameObject characterScreen;
    static GameObject skillScreen;

    static GameObject bankScreen;

    static GameObject controlObject;
    static GameObject centerEye;

    public static bool bankActive;
    // Use this for initialization
    void Start()
    {
        controlObject = GameObject.Find("Control");

        centerEye = GameObject.Find("CenterEyeAnchor");

        inventoryScreen = GameObject.Find("InventoryScreen");
        debugLog = GameObject.Find("DebugLog");
        characterScreen = GameObject.Find("CharacterScreen");
        skillScreen = GameObject.Find("SkillScreen");

        bankScreen = GameObject.Find("BankScreen");
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            if (EquippedController.selectedObject.layer == 21 && EquippedController.selectedObject.name == "bank") {
                controlObject.transform.rotation = centerEye.transform.rotation;
                bankActive = true;
            }
        }

        if (bankActive)
        {
            //LocalNotification.Send("Update!");
            if (!inventoryScreen.activeInHierarchy)
            {
                inventoryScreen.SetActive(true);
            }
            if (!bankScreen.activeInHierarchy)
            {
                bankScreen.SetActive(true);
            }

            if (debugLog.activeInHierarchy)
            {
                debugLog.SetActive(false);
            }
            if (characterScreen.activeInHierarchy)
            {
                characterScreen.SetActive(false);
            }
            if (skillScreen.activeInHierarchy)
            {
                skillScreen.SetActive(false);
            }
        }
    }
}
