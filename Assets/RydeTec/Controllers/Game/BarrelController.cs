﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelController : MonoBehaviour {

    GameObject barrel;

    bool isOpen;

	// Use this for initialization
	void Start () {
        barrel = GameObject.Find("newbarrel");

        isOpen = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger)) {
            OVRDebug.Log(EquippedController.selectedObject.transform.parent.name + " clicked");
            if (EquippedController.selectedObject.layer == 21 && EquippedController.selectedObject.transform.parent.name == barrel.name)
            {
                OVRDebug.Log("Animating Barrel.");
                Animator animator = barrel.GetComponent<Animator>();

                if (!isOpen) {
                    animator.Play("open_barrel");
                    isOpen = true;
                } else {
                    animator.Play("idle_barrel");
                    isOpen = false;
                }

            }
        }
	}
}
