﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ServerCharacterController: MonoBehaviour {
    public static List<Character> characters = new List<Character>();

    public static GameObject baseModel;

    public static List<GameObject> models = new List<GameObject>();

    public static Character character;

    private void Start()
    {
        baseModel = GameObject.Find("testbarrel");
    }

	private void FixedUpdate()
	{
        for (int c = 0; c < characters.Count; c++)
        {
            if (characters[c].needToMove)
            {
                StopCoroutine(characters[c].move_until_done());
                StartCoroutine(characters[c].move_until_done());
                characters[c].needToMove = false;
            }
        }
	}

	private void Update()
    {
        for (int m = 0; m < models.Count; m++){
            models[m].SetActive(false);
        }

        //OVRDebug.Log("Characters: " + characters.Count);
        for (int c = 0; c < characters.Count; c++){

            characters[c].update();


            if (characters[c].name.Equals(ServerCharacterController.character.name) || characters[c].name == "") {

                for (int m = 0; m < models.Count; m++)
                {
                    if (models[m].name.Equals(characters[c].name))
                    {
                        models[m].SetActive(false);

                        break;
                    }
                }
                continue;
            }

            bool found = false;
            for (int m = 0; m < models.Count; m++){
                if (models[m].name.Equals(characters[c].name) && characters[c].name != ServerCharacterController.character.name){
                    found = true;

                    models[m].transform.position = new Vector3(characters[c].pos[0],
                                                                                     characters[c].pos[1],
                                                                                     characters[c].pos[2]);
                    models[m].SetActive(true);

                    break;
                }
            }

            if (!found){
                OVRDebug.Log("New Character " + characters[c].name);
                GameObject newModel = Instantiate(baseModel, baseModel.transform.position, baseModel.transform.rotation);

                newModel.name = characters[c].name;
                models.Add(newModel);

                newModel.transform.position = new Vector3(characters[c].pos[0],
                                                          characters[c].pos[1],
                                                          characters[c].pos[2]);
                newModel.SetActive(true);

                OVRDebug.Log("Done New Character");
            } else {
                continue;
            }
        }
    }
}
