﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using Oculus;
using Oculus.Platform;
using Oculus.Platform.Models;


public class CommandController
{
    public static Queue<SimpleJSON.JSONNode> commandQueue = new Queue<SimpleJSON.JSONNode>();

    public static string incoming_data = "";

    public static SimpleJSON.JSONNode parse_command(string partialData) {

        incoming_data += partialData;

        string testString = "";
        bool doneWithData = false;
        bool corrupt = false;

        int opens = 0;
        int closes = 0;

        foreach (char c in incoming_data) {
            if (!doneWithData)
            {
                testString += c;

                if (c == '{') opens++;
                if (c == '}') closes++;

                if (opens == closes)
                {
                    doneWithData = true;
                    incoming_data = "";
                }

                if (closes > opens)
                {
                    OVRDebug.Log("Trashing: " + incoming_data);
                    doneWithData = true;
                    corrupt = true;
                    incoming_data = "";
                }
            } else {
                incoming_data += c;
            }
        }

        if (corrupt) {
            return null;
        }

        SimpleJSON.JSONNode command = null;

        try {
            command = SimpleJSON.JSON.Parse(testString);
        } catch (Exception e) {
            OVRDebug.Log("DIDN'T CATCH CORRUPT PROTOCOL! " + testString);
        }

        return command;


        //for (int i = 0; i < chars.Length; i++) {
        //    testString += chars[i];
        //    //OVRDebug.Log("Testing: " + testString);
        //    if (chars[i] == '{') {
        //        opens += 1;
        //    }
        //    if (chars[i] == '}') {
        //        closes += 1;
        //    }
        //    SimpleJSON.JSONNode command = null;
        //    try
        //    {
        //        command = SimpleJSON.JSON.Parse(testString);
        //    } catch (Exception e) {
        //        continue;
        //    }

        //    if (opens == closes) {
        //        //OVRDebug.Log("Parse Done!");
        //        incoming_data = incoming_data.Substring(i + 1);
        //        return command;
        //    }

        //    if (closes > opens) {
        //        OVRDebug.Log("Trashing: " + incoming_data);
        //        incoming_data = "";
        //        return null;
        //    }
        //}
        //return null;
    }



    public static void handle(string data)
    {
        SimpleJSON.JSONNode command = null;
        //OVRDebug.Log("Handle: " + data);

        try
        {
            command = parse_command(data);
        }
        catch (Exception e)
        {
            OVRDebug.Log("Error: " + e);
            return;
        }

        if (command == null)
        {
            OVRDebug.Log("Null");
            return;
        }

        //OVRDebug.Log("Queued: " + command.ToString());
        commandQueue.Enqueue(command);


    }

    public static void runNextCommand() {

        if (commandQueue.Count == 0) {
            return;
        }

        SimpleJSON.JSONNode command = commandQueue.Dequeue();
        //OVRDebug.Log("Dequeued: " + command.ToString());

        if (command["verified"] != null)
        {
            OVRDebug.Log("Verified command recieved");
            if (command["verified"]["alter"]["new"]["character"] != null)
            {
                TitleController.waitingForServerVerify = false;
            }
            if (command["verified"]["login"] != null)
            {
                TitleController.waitingForServerVerify = false;
            }

            if (command["verified"]["action"] != null){
                OVRDebug.Log("Got skill response!");
                OVRDebug.Log(command["verified"]["action"]);

                if (command["verified"]["action"]["skill"] != null) {
                    if (command["verified"]["action"]["skill"] == "Woodcutting") {
                        if (command["verified"]["action"]["didHit"] == "true")
                        {
                            OVRDebug.Log("Hit the tree!");
                            WoodcuttingController.ShouldHit();

                            if (command["verified"]["action"]["reward"] != null)
                            {
                                OVRDebug.Log("Got reward!");
                                string xp = command["verified"]["action"]["reward"]["xp"];
                                string item_name = command["verified"]["action"]["reward"]["item"];

                                LocalNotification.Send("+" + xp + " xp");
                                LocalNotification.Send("+1 " + item_name);
                            }

                        }
                        else
                        {
                            WoodcuttingController.ShouldMiss();
                        }
                    }
                }
            }
        }



        if (command["title"] != null)
        {
            //OVRDebug.Log(command["title"]["servers"][0].Value);
            //OVRDebug.Log(command["title"]["characters"].Count);

            for (int i = 0; i < command["title"]["characters"].Count; i++)
            {
                TitleController.characters[i] = command["title"]["characters"][i];
            }

            for (int i = 0; i < command["title"]["servers"].Count; i++)
            {
                TitleController.servers[i] = command["title"]["servers"][i].Value;
            }
        }

        // TODO: Instant crash here! Unstable.
        try
        {
            if (command["state"] != null)
            {
                if (NetworkSystem.inGame)
                {
                    //OVRDebug.Log("State: " + command_strings[co]);
                    if (command["state"]["inventory"] != null)
                    {
                        for (int i = 0; i < command["state"]["inventory"].Count; i++)
                        {
                            if (command["state"]["inventory"][i]["name"] != null)
                            {
                                Item item = new Item(
                                    command["state"]["inventory"][i]["name"],
                                    command["state"]["inventory"][i]["stackable"],
                                    command["state"]["inventory"][i]["quantity"],
                                    command["state"]["inventory"][i]["price"]);

                                RydePlayerController.items[i] = item;
                            }
                            else
                            {
                                RydePlayerController.items[i] = null;
                            }
                        }
                    }

                    //OVRDebug.Log("State: " + command_strings[co]);
                    if (command["state"]["world"] != null)
                    {
                        for (int i = 0; i < command["state"]["world"].Count; i++)
                        {
                            if (command["state"]["world"][i]["name"] != null)
                            {
                                Item item = new Item(
                                    command["state"]["world"][i]["name"],
                                    command["state"]["world"][i]["stackable"],
                                    command["state"]["world"][i]["quantity"],
                                    command["state"]["world"][i]["price"]);

                                WorldItem worldItem = new WorldItem(item, new Vector3(
                                    command["state"]["world"][i]["position"][0],
                                    command["state"]["world"][i]["position"][1],
                                    command["state"]["world"][i]["position"][2]));

                                RydePlayerController.worldItems[i] = worldItem;
                            }
                            else
                            {
                                RydePlayerController.worldItems[i] = null;
                            }
                        }
                    }
                }



                //OVRDebug.Log("State command recieved");
                if (command["state"]["characters"] != null)
                {
                    //OVRDebug.Log("characters command recieved");
                    for (int i = 0; i < command["state"]["characters"].Count; i++)
                    {
                        if (command["state"]["characters"][i] != null)
                        {
                            if (command["state"]["characters"][i]["name"] != null || ServerCharacterController.character != null)
                            {
                                if (NetworkSystem.inGame)
                                {
                                    if (command["state"]["characters"][i]["name"] != ServerCharacterController.character.name)
                                    {

                                        bool found = false;

                                        for (int f = 0; f < ServerCharacterController.characters.Count; f++)
                                        {
                                            if (ServerCharacterController.characters[f].name.Equals(command["state"]["characters"][i]["name"]))
                                            {
                                                found = true;

                                                ServerCharacterController.characters[f].setPosition(
                                                    (float)command["state"]["characters"][i]["x"],
                                                    (float)command["state"]["characters"][i]["y"],
                                                    (float)command["state"]["characters"][i]["z"]);
                                            }
                                        }

                                        if (!found)
                                        {
                                            Character character = new Character(
                                                command["state"]["characters"][i]["name"],
                                                (float)command["state"]["characters"][i]["x"],
                                                (float)command["state"]["characters"][i]["y"],
                                                (float)command["state"]["characters"][i]["z"]);

                                            ServerCharacterController.characters.Add(character);
                                        }
                                    }
                                }
                                else
                                {
                                    if (command["state"]["characters"][i]["name"].Equals(ServerCharacterController.character.name))
                                    {
                                        ServerCharacterController.character.pos = new float[] {
                                        (float)command["state"]["characters"][i]["x"],
                                        (float)command["state"]["characters"][i]["y"],
                                        (float)command["state"]["characters"][i]["z"]};

                                        OVRDebug.Log("Setting player position.");
                                        NetworkSystem.inGame = true;
                                        NetworkSystem.readyForPlayer = true;
                                    }
                                }
                            }
                        }
                    }
                }

            }



        }
        catch (Exception e)
        {
            OVRDebug.Log("State: " + e);
        }
    }
}