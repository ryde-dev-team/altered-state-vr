using UnityEngine;
using System;
using System.Collections;
using Oculus;
using Oculus.Platform;
using Oculus.Platform.Models;
using System.Net.Sockets;
using System.Text;
using System.Threading;


public class NetworkController : MonoBehaviour
{

    public class NetworkConfig
    {
        public static string server = "192.168.0.2";
        public static int port = 49867;
    }

    public class RydeAccount
    {
        public static string username = "";
    }

    public class TCPClient {
        public static TcpClient client;
        public static Thread thread;

        public static void Listen() {
            try {
                client = new TcpClient(NetworkConfig.server, NetworkConfig.port);
                RydeDebug.Send("Client listening...");

                Send(RydeProtocol.Hello(RydeAccount.username));
            } catch (Exception e) {
                RydeDebug.Send("Client failed to start " + e);
            }

            Byte[] bytes = new Byte[1024];
            while (true) {
                using (NetworkStream stream = client.GetStream()) {
                    int length;

                    while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        var incommingData = new byte[length];
                        Array.Copy(bytes, 0, incommingData, 0, length);
                        // Convert byte array to string message.                        
                        string serverMessage = Encoding.ASCII.GetString(incommingData);
                        RydeCommand.handle(serverMessage);
                    }
                }
            }
        }

        public static void Send(string data) {
            try
            {
                // Get a stream object for writing.             
                NetworkStream stream = client.GetStream();
                if (stream.CanWrite)
                {
                    // Convert string message to byte array.                 
                    byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(data);
                    // Write byte array to socketConnection stream.                 
                    stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
                }
            }
            catch (SocketException socketException)
            {
                RydeDebug.Send("Network: " + socketException);
            }
        }
    }

    void RydeLogin(Message<User> message)
    {
        RydeDebug.Send("Got login message back.");
        if (!message.IsError)
        {
            RydeAccount.username = message.Data.OculusID;
            RydeDebug.Send(RydeAccount.username + " logged in.");

            try {
                TCPClient.thread = new Thread(new ThreadStart(TCPClient.Listen));
                TCPClient.thread.IsBackground = true;
                TCPClient.thread.Start();
            }catch (Exception e)
            {
                RydeDebug.Send("Network: " + e);
            }
        }
        else
        {
            RydeDebug.Send("Error logging in");
        }
    }

	// Use this for initialization
	void Start()
	{
        RydeDebug.Send("NetworkController started.");

        try
        {
            Core.Initialize();
            RydeDebug.Send("Oculus platform core initialized");
        }
        catch (Exception e)
        {
            RydeDebug.Send(e.ToString());
        }


        Users.GetLoggedInUser().OnComplete(RydeLogin);
        RydeDebug.Send("Start oculus user");
	}



	// Update is called once per frame
	void Update()
	{
			
	}
}
