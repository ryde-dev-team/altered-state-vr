﻿using UnityEngine;
using System.Collections;

public class EquippedController : MonoBehaviour
{
    public static GameObject selectedObject;

    public static GameObject goRemote;
    public static GameObject miningRemote;
    public static GameObject woodcuttingRemote;


    // Use this for initialization
    void Start()
    {
        goRemote = GameObject.Find("OculusGoControllerModel");
        miningRemote = GameObject.Find("pickaxe");
        woodcuttingRemote = GameObject.Find("axe");

        miningRemote.SetActive(false);
        woodcuttingRemote.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {

        if (MiningController.modeActive) {
            goRemote.SetActive(false);
            miningRemote.SetActive(true);
            woodcuttingRemote.SetActive(false);
        } else if (WoodcuttingController.modeActive) {
            goRemote.SetActive(false);
            miningRemote.SetActive(false);
            woodcuttingRemote.SetActive(true);
        } else {
            goRemote.SetActive(true);
            miningRemote.SetActive(false);
            woodcuttingRemote.SetActive(false);
        }


        LineRenderer lr = GameObject.Find("LaserPointer").GetComponent<LineRenderer>();
        lr.SetPosition(0, Vector3.zero);
        lr.SetPosition(1, Vector3.zero);
        selectedObject = null;

        if (!WoodcuttingController.modeActive && !MiningController.modeActive && !RydePlayerController.moving)
        {
            Camera cam = GameObject.Find("LaserPointer").GetComponent<Camera>();
            Ray ray = new Ray(cam.transform.position, cam.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 25))
            {
                if (hit.collider != null && (hit.collider.gameObject.layer >= 18 || hit.collider.gameObject.layer == 5))
                {
                    selectedObject = hit.collider.gameObject;

                    lr.SetPosition(0, GameObject.Find("LaserPointer").transform.position);
                    lr.SetPosition(1, hit.point);
                }
            }
        }

        if (!RydePlayerController.moving)
        {
            if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
            {
                if (selectedObject.layer == 18)
                {
                    OVRDebug.Log("Entered Woodcutting Mode");
                    WoodcuttingController.modeActive = true;
                    WoodcuttingController.currentTree = selectedObject;
                }
                else if (selectedObject.layer == 19)
                {
                    OVRDebug.Log("Entered Mining Mode");
                    MiningController.modeActive = true;
                    MiningController.currentOre = selectedObject;
                }

            }
            else
            {
                WoodcuttingController.modeActive = false;
                MiningController.modeActive = false;
            }
        }

    }
}
