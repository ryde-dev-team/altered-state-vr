﻿using UnityEngine;
using System.Collections;

public class InventoryScreenController : MonoBehaviour
{
    static GameObject inventoryScreen;
    GameObject[] slots;

    Texture2D blankSlot;
    // Use this for initialization
    void Start()
    {
        inventoryScreen = GameObject.Find("InventoryScreen");

        slots = new GameObject[21];

        for (int i = 0; i < 21; i++)
        {
            slots[i] = inventoryScreen.transform.GetChild(i).gameObject;
        }

        blankSlot = Resources.Load("blank_slot") as Texture2D;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < RydePlayerController.items.Length; i++) {
            Item item = RydePlayerController.items[i];
            if (item != null)
            {
                if (item.stack <= 0)
                {
                    OVRDebug.Log("Item purged from inventory.");
                    RydePlayerController.items[i] = null;
                    slots[i].GetComponentInChildren<TextMesh>().text = "";
                } else {
                    Texture2D itemTexture = Resources.Load(item.name) as Texture2D;

                    if (itemTexture != null) {
                        slots[i].GetComponent<Renderer>().material.mainTexture = itemTexture;
                    } else {
                        slots[i].GetComponent<Renderer>().material.mainTexture = blankSlot;
                        OVRDebug.Log("No texture for " + item.name);
                    }

                    slots[i].GetComponentInChildren<TextMesh>().text = item.name; 
                    if (item.stack > 1)
                    {
                        slots[i].GetComponentInChildren<TextMesh>().text = item.name + "\n" + item.stack;
                    }
                }
            } else {
                slots[i].GetComponent<Renderer>().material.mainTexture = blankSlot;
                slots[i].GetComponentInChildren<TextMesh>().text = "";
            }
        }

        if (BankController.bankActive) {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            {
                if (EquippedController.selectedObject.transform.parent.gameObject.name == "InventoryScreen")
                {
                    for (int i = 0; i < slots.Length; i++)
                    {
                        if (EquippedController.selectedObject.name == "slot" + i &&
                            slots[i].GetComponentInChildren<TextMesh>().text != "")
                        {
                            OVRDebug.Log(slots[i].GetComponentInChildren<TextMesh>().text + " selected");

                            Item item = RydePlayerController.items[i];

                            int rm = RydePlayerController.addItemToBank(item);

                            if (rm >= 1)
                            {
                                if (RydePlayerController.items[i].stack <= 1)
                                {
                                    RydePlayerController.items[i] = null;
                                } else {
                                    if (RydePlayerController.items[i].stackable)
                                    {
                                        RydePlayerController.items[i] = null;
                                    } else {
                                        RydePlayerController.items[i].removeAmount(rm);
                                    }

                                }
                                OVRDebug.Log("Item stored in bank.");
                            }
                             else {
                                LocalNotification.Send("Your bank is full!");
                            }

                            break;
                        }
                    }
                }
            }
        } else if (StoreController.modeActive) {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            {
                if (EquippedController.selectedObject.transform.parent.gameObject.name == "InventoryScreen")
                {
                    for (int i = 0; i < slots.Length; i++)
                    {
                        if (EquippedController.selectedObject.name == "slot" + i &&
                            slots[i].GetComponentInChildren<TextMesh>().text != "")
                        {
                            OVRDebug.Log(slots[i].GetComponentInChildren<TextMesh>().text + " selected");

                            Item item = RydePlayerController.items[i];

                            int rm = StoreController.Sell(item);

                            if (rm >= 1)
                            {
                                if (RydePlayerController.items[i].stack <= 1)
                                {
                                    RydePlayerController.items[i] = null;
                                }
                                else
                                {
                                    if (RydePlayerController.items[i].stackable)
                                    {
                                        RydePlayerController.items[i] = null;
                                    }
                                    else
                                    {
                                        RydePlayerController.items[i].removeAmount(rm);
                                    }

                                }
                                RydePlayerController.addItemToInventory(new Item("Copper", true, rm * item.value));
                                OVRDebug.Log("Item sold.");
                            }
                            else
                            {
                                LocalNotification.Send("The store is full!");
                            }

                            break;
                        }
                    }
                }
            }
        } else {

            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            {
                if (EquippedController.selectedObject.transform.parent.gameObject.name == "InventoryScreen")
                {
                    for (int i = 0; i < slots.Length; i++)
                    {
                        if (EquippedController.selectedObject.name == "slot" + i &&
                            slots[i].GetComponentInChildren<TextMesh>().text != "")
                        {
                            OVRDebug.Log(RydePlayerController.items[i].name + " dropped");

                            NetworkSystem.Command.Data[] target = { new NetworkSystem.Command.Data("name", RydePlayerController.items[i].name), new NetworkSystem.Command.Data("source", "inventory") };
                            NetworkSystem.Command.Data[] item = { new NetworkSystem.Command.Data("item", target) };
                            NetworkSystem.Command.Data[] move = { new NetworkSystem.Command.Data("move", item) };

                            NetworkSystem.Command command = new NetworkSystem.Command(NetworkSystem.Command.Protocol.alter, move);

                            NetworkSystem.SendCommand(command);

                        }
                    }
                }
            }


        }

    }
}
