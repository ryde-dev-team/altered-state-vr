﻿using UnityEngine;
using System.Collections;

public class BankScreenController : MonoBehaviour
{
    public static GameObject bankScreen;
    GameObject[] slots;
    Texture2D blankSlot;
    // Use this for initialization
    void Start()
    {
        bankScreen = GameObject.Find("BankScreen");
        slots = new GameObject[60];

        for (int i = 0; i < 60; i++) {
            slots[i] = bankScreen.transform.GetChild(i).gameObject;

            //OVRDebug.Log(i + ": " + slots[i].name);
        }

        blankSlot = Resources.Load("blank_slot") as Texture2D;
    }

    // Update is called once per frame
    void Update()
    {
        if (BankController.bankActive)
        {
            for (int i = 0; i < RydePlayerController.bank.Length; i++)
            {
                Item item = RydePlayerController.bank[i];




                if (item != null)
                {

                    if (item.name == "Copper" && item.stack >= 1000)
                    {
                        OVRDebug.Log("10000 Copper > 1 Silver");
                        if (RydePlayerController.removeItemFromBank(new Item("Copper", true, 1000)))
                        {
                            RydePlayerController.addItemToBank(new Item("Silver", true, 1, 1000));
                        }
                    }

                    if (item.name == "Silver" && item.stack >= 1000)
                    {
                        OVRDebug.Log("10000 Silver > 1 Gold");
                        if (RydePlayerController.removeItemFromBank(new Item("Silver", true, 1000)))
                        {
                            RydePlayerController.addItemToBank(new Item("Gold", true, 1, 1000*1000));
                        }
                    }

                    if (item.name == "Gold" && item.stack >= 1000)
                    {
                        OVRDebug.Log("10000 Gold > 1 Platinum");
                        if (RydePlayerController.removeItemFromBank(new Item("Gold", true, 1000)))
                        {
                            RydePlayerController.addItemToBank(new Item("Platinum", true, 1, 1000*1000*1000));
                        }
                    }


                    if (item.stack <= 0)
                    {
                        OVRDebug.Log("Item purged from bank.");
                        RydePlayerController.bank[i] = null;
                        slots[i].GetComponentInChildren<TextMesh>().text = "";
                    }
                    else
                    {
                        Texture2D itemTexture = Resources.Load(item.name) as Texture2D;

                        if (itemTexture != null)
                        {
                            slots[i].GetComponent<Renderer>().material.mainTexture = itemTexture;
                        }
                        else
                        {
                            slots[i].GetComponent<Renderer>().material.mainTexture = blankSlot;
                            OVRDebug.Log("No texture for " + item.name);
                        }

                        slots[i].GetComponentInChildren<TextMesh>().text = item.name;
                        if (item.stack > 1)
                        {
                            slots[i].GetComponentInChildren<TextMesh>().text = item.name + "\n" + item.stack;
                        }
                    }
                }
                else
                {
                    slots[i].GetComponent<Renderer>().material.mainTexture = blankSlot;
                    slots[i].GetComponentInChildren<TextMesh>().text = "";
                }
            }
        }else if (StoreController.modeActive) {
            for (int i = 0; i < StoreController.items.Length; i++)
            {
                Item item = StoreController.items[i];




                if (item != null)
                {

                    if (item.stack <= 0)
                    {
                        OVRDebug.Log("Item purged from store.");
                        StoreController.items[i] = null;
                        slots[i].GetComponentInChildren<TextMesh>().text = "";
                    }
                    else
                    {
                        Texture2D itemTexture = Resources.Load(item.name) as Texture2D;

                        if (itemTexture != null)
                        {
                            slots[i].GetComponent<Renderer>().material.mainTexture = itemTexture;
                        }
                        else
                        {
                            slots[i].GetComponent<Renderer>().material.mainTexture = blankSlot;
                            OVRDebug.Log("No texture for " + item.name);
                        }

                        slots[i].GetComponentInChildren<TextMesh>().text = item.name + "\n" + item.value;
                        if (item.stack > 1)
                        {
                            slots[i].GetComponentInChildren<TextMesh>().text = item.name + "\n" + item.stack + "x" + item.value;
                        }
                    }
                }
                else
                {
                    slots[i].GetComponent<Renderer>().material.mainTexture = blankSlot;
                    slots[i].GetComponentInChildren<TextMesh>().text = "";
                }
            }
        }

        if (OVRInput.GetDown(OVRInput.Button.One)) {
            if (EquippedController.selectedObject.transform.parent.gameObject.name == "BankScreen")
            {
                for (int i = 0; i < slots.Length; i++)
                {
                    if (EquippedController.selectedObject.name == "slot" + i &&
                        slots[i].GetComponentInChildren<TextMesh>().text != "")
                    {
                        OVRDebug.Log(slots[i].GetComponentInChildren<TextMesh>().text + " selected");

                        Item item = RydePlayerController.bank[i];

                        if (!BankController.bankActive && StoreController.modeActive)
                        {
                            item = StoreController.items[i];
                        }

                        LocalNotification.Send(item.name + " x" + item.stack + " value: " + item.value);

                        break;
                    }
                }
            }

        }

        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            if (EquippedController.selectedObject.transform.parent.gameObject.name == "BankScreen")
            {
                for (int i = 0; i < slots.Length; i++)
                {
                    if (EquippedController.selectedObject.name == "slot" + i &&
                        slots[i].GetComponentInChildren<TextMesh>().text != "")
                    {
                        OVRDebug.Log(slots[i].GetComponentInChildren<TextMesh>().text + " selected");

                        Item item = RydePlayerController.bank[i];

                        if (!BankController.bankActive && StoreController.modeActive) {
                            item = StoreController.items[i];
                        }

                        if (StoreController.modeActive) {
                            if (!RydePlayerController.removeMoney(item.value)) {
                                break;
                            }
                        }

                        int rm = RydePlayerController.addItemToInventory(new Item(item.name, item.stackable, 1, item.value));

                        if (rm >= 1)
                        {
                            if (BankController.bankActive) {
                                RydePlayerController.bank[i].removeAmount(rm);
                                OVRDebug.Log("Item withdrawn.");
                            }

                            if (StoreController.modeActive) {
                                //TCPTestClient.Send("Bought " + rm + " " + StoreController.items[i].name + " from the store.");
                                StoreController.items[i].removeAmount(rm);
                                OVRDebug.Log("Item Bought");
                            }

                        }
                        else
                        {
                            LocalNotification.Send("Your inventory is full!");
                        }

                        break;
                    }
                }
            }
        }

    }
}
