using UnityEngine;
using System.Collections;

public class RydeInterfaceController : MonoBehaviour
{

    public static GameObject debug;
    public static GameObject character;
    public static GameObject skill;
    public static GameObject inventory;
    public static GameObject storage;


	// Use this for initialization
	void Start()
	{
        debug = GameObject.Find("DebugLog");
        character = GameObject.Find("CharacterScreen");
        skill = GameObject.Find("SkillScreen");
        inventory = GameObject.Find("InventoryScreen");
        storage = GameObject.Find("BankScreen");

        character.SetActive(false);
        skill.SetActive(false);
        inventory.SetActive(false);
        storage.SetActive(false);
	}

	// Update is called once per frame
	void Update()
	{
			
	}
}
