using UnityEngine;
using System.Collections;
using System;

public class Skill : ScriptableObject
{
    public new string name;
    public int level;
    public int current;

    public int currentXP;
    public int nextLevelXP;

    public Skill(string n, int l, int c, int cxp)
    {
        name = n;
        level = l;
        current = c;
        currentXP = cxp;
        nextLevelXP = 0;

        calculateNextXP();
    }

    public enum Type
    {
        health,
        attack,
        strength,
        defense,
        archery,
        magic,
        stealth,
        woodcutting,
        fishing,
        mining,
        gathering,
        cooking,
        blacksmithing,
        crafting,
        farming,
        alchemy
    }


    private void calculateNextXP()
    {
        double nlxp = 0;
        for (int i = 1; i <= level; i++)
        {
            nlxp += (i + (300 * (Math.Pow(2.0, (i / 7.0)))));
        }
        nlxp = Math.Floor(nlxp) / 4.0;
        nextLevelXP = (int)Math.Floor(nlxp);
    }

    private bool levelUp()
    {
        if (currentXP >= nextLevelXP)
        {

            level += 1;
            current += 1;

            calculateNextXP();

            if (!levelUp())
            {
                LocalNotification.Send(name + " Level " + level);
                OVRDebug.Log(name + " leveled up to " + level);


                return true;
            }
        }
        return false;
    }

    public void gain(int val)
    {
        currentXP += val;

        levelUp();
    }
}
