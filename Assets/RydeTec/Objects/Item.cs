using UnityEngine;
using System.Collections;

public class Item
{
    public string name;
    public bool stackable;
    public int stack;
    public int value;

    private int maxStack;

    public Item(string n, bool canStack = false, int quanity = 1, int price = 1)
    {
        name = n;
        stackable = canStack;
        stack = quanity;
        value = price;

        if (stackable)
        {
            maxStack = 1000000000;
        }
        else
        {
            maxStack = 1;
        }
    }

    public int addAmount(int val)
    {
        stack += val;

        if (stack > maxStack)
        {
            stack -= val;
            int real = val - (val - maxStack);
            stack += real;
            return real;
        }

        return val;
    }

    public int removeAmount(int val)
    {
        stack -= val;

        if (stack < 1)
        {
            stack += val;
            int real = val - (val - stack);
            stack = 0;
            return real;
        }

        return val;
    }

}

public class WorldItem
{

    public Item item;
    public GameObject gameObject;

    public WorldItem(Item item, Vector3 position){
        this.item = item;

        gameObject = GameObject.Instantiate(GameObject.Find("item_" + item.name));
        gameObject.transform.position = position;
    }
}
