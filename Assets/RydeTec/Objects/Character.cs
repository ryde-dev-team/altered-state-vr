﻿using Unity;
using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
public class Character
{

    public int id;
    public string name;
    public float[] pos = new float[3] { 0.0f, 0.0f, 0.0f };
    public float[] lastPosition = new float[] { 0.0f, 0.0f, 0.0f };

    public float[] target = new float[] { 0.0f, 0.0f, 0.0f };
    public float remaining_time = 0;
    //public float step_per_milli = 0;

    public float lastServerUpdate = 0f;

    public bool needToMove = false;

    public Character(string name, float x, float y, float z)
    {
        //this.id = id;
        this.name = name;

        this.pos = new float[3] { x, y, z };
    }



    public void update() {
        float currentTime = Time.realtimeSinceStartup;
        if (!this.name.Equals(ServerCharacterController.character.name)) {
            //OVRDebug.Log("Not player");
        }

    }

    public void move_towards_target(float step) {
        float[] dir = MathingSystem.calculateDirection(pos, target);
        this.pos[0] = this.pos[0] - dir[0] * step;
        this.pos[2] = this.pos[2] - dir[1] * step;
    }

    public void auto_move_to(float[] target, float time) {
        this.target = target;
        this.remaining_time = time;

        //float dis = MathingSystem.calculateDistance(pos, target);

        //step_per_milli = dis / (time * 1000f);

        this.needToMove = true;
    }

    public IEnumerator move_until_done() {
        while (this.pos != lastPosition && !this.name.Equals(ServerCharacterController.character.name))
        {
            //OVRDebug.Log("Updating " + this.name);
            if (remaining_time > 0)
            {
                remaining_time = remaining_time - Time.fixedDeltaTime;

                //OVRDebug.Log("Remaining " + remaining_time);

                if (remaining_time <= 0)
                {
                    remaining_time = 0;
                    pos = lastPosition;
                    yield break;
                }
                else
                {
                    float dis = MathingSystem.calculateDistance(pos, target);

                    if (dis <= 0.0)
                    {
                        remaining_time = 0;
                        pos = lastPosition;
                        yield break;
                    }
                    else
                    {
                        float velStep = (dis / (remaining_time * 1000f)) * (Time.fixedDeltaTime * 1000f);
                        move_towards_target(velStep);
                        yield return null;
                    }
                }
            }
        }
    }

    public void setPosition(float x, float y, float z) {
        
        if (ServerCharacterController.character != null) {
            if (this.name == ServerCharacterController.character.name){
                this.pos = new float[3] { x, y, z };

                if (MathingSystem.calculateDistance(lastPosition, this.pos) >= 1 && Time.realtimeSinceStartup - lastServerUpdate >= 0.6f)
                {
                    NetworkSystem.Command command = new NetworkSystem.Command(NetworkSystem.Command.Protocol.alter,
                                                                                  new NetworkSystem.Command.Data[] {
                                new NetworkSystem.Command.Data("character",
                                                               new NetworkSystem.Command.Data[] {
                            new NetworkSystem.Command.Data("position", new NetworkSystem.Command.Data[] {
                                new NetworkSystem.Command.Data("" + x),
                                new NetworkSystem.Command.Data("" + y),
                                new NetworkSystem.Command.Data("" + z)
                            })
                                })
                            });

                    NetworkSystem.SendCommand(command);

                    lastPosition = this.pos;
                    lastServerUpdate = (float) Time.realtimeSinceStartup;
                }
            } else {
                lastPosition = new float[3] { x, y, z };
                if (MathingSystem.calculateDistance(lastPosition, this.pos) >= 1)
                {
                    auto_move_to(lastPosition, 0.6f);
                }
            }
        }      
    }
}
