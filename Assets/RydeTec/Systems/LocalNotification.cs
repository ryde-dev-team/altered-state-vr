﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class LocalNotification
{
    public static List<string> logs = new List<string>();
    public static string text;
    public static int nLogs = 16; // max number of lines to display. Can be changed


    static LocalNotification()
    {
        text = "";
    }

    public static void Send(object log)
    {
        // add the log to the queue
        string s = log.ToString();
        logs.Add(s);
        // make sure we don't keep too many logs
        if (logs.Count > nLogs)
            logs.RemoveAt(0);
        PrintLogs();
    }

    private static void PrintLogs()
    {
        string s = "";
        foreach (string k in logs)
        {
            s += k + "\n";
        }
        text = s;
    }
}
