using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using Oculus;
using Oculus.Platform;
using Oculus.Platform.Models;

public class NetworkSystem
{

    public class Command {
        public enum Protocol {
            hello,
            latency,
            hop,
            login,
            alter,
            state,
            title,
            verify
        }

        public class Data {
            public string key;
            public Data[] data;

            public Data(string key) {
                this.key = key;
            }

            public Data(string key, string data) {
                this.key = key;
                this.data = new Data[] { new Data(data) };
            }

            public Data(string key, Data[] data) {
                this.key = key;
                this.data = data;
            }

            public string toString(bool brackets = true) {
                string ret_string = "";

                if (data != null && brackets) {
                    ret_string += "{ ";
                }

                ret_string += '"' + this.key + '"';


                if (data != null)
                {
                    if (data[0].data == null)
                    {
                        if (data.Length == 1)
                        {
                            ret_string += ":" + data[0].toString();
                        }
                        else
                        {
                            ret_string += ":[" + '"' + data[0].key + '"';
                            for (int i = 1; i < data.Length; i++)
                            {
                                ret_string += ",";
                                ret_string += '"' + data[i].key + '"';
                            }
                            ret_string += "]";
                        }
                    }
                    else
                    {
                        if (data.Length == 1)
                        {
                            ret_string += ":" + data[0].toString();
                        }
                        else
                        {
                            ret_string += ":{" + data[0].toString(false);
                            for (int i = 1; i < data.Length; i++)
                            {
                                ret_string += ",";
                                ret_string += data[i].toString(false);
                            }
                            ret_string += "}";
                        }
                    }


                    if (brackets) {
                        ret_string += "}";
                    }
                }



                return ret_string;
            }
        }

        Protocol protocol;
        Data[] data;

        public Command(Protocol protocol, Data[] data) {
            this.protocol = protocol;
            this.data = data;


            //SendCommand(
            //    new Command(
            //        Command.Protocol.hello,
            //        new Command.Data[] {
            //        new Command.Data("oculusID", oculusID),
            //        new Command.Data("test", new Command.Data[] {
            //            new Command.Data("test1"),
            //            new Command.Data("test2")
            //        }),
            //        new Command.Data("test3", new Command.Data[] {
            //            new Command.Data("test4", new Command.Data[] {
            //            new Command.Data("test6"),
            //            new Command.Data("test7")
            //            }),
            //            new Command.Data("test5", new Command.Data[] {
            //            new Command.Data("test8"),
            //            new Command.Data("test9")
            //            })
            //        })
            //    }
            //    )
            //);
        }

        public string toString() {
            string ret_string = "";

            ret_string += "{" + '"' + protocol + '"';

            if (data[0].data == null) {
                if (data.Length == 1)
                {
                    ret_string += ":" + data[0].toString();
                }
                else
                {
                    ret_string += ":[" + '"' + data[0].key + '"';
                    for (int i = 1; i < data.Length; i++)
                    {
                        ret_string += ",";
                        ret_string += '"' + data[i].key + '"';
                    }
                    ret_string += "]";
                }
            } else {
                if (data.Length == 1)
                {
                    ret_string += ":" + data[0].toString();
                } else {
                    ret_string += ":{" + data[0].toString(false);
                    for (int i = 1; i < data.Length; i++)
                    {
                        ret_string += ",";
                        ret_string += data[i].toString(false);
                    }
                    ret_string += "}";
                }
            }



            ret_string += "}";

            return ret_string;
        }
    }


    public static bool isStarted = false;
    public static bool inGame = false;

    private static TcpClient client;
    private static Thread thread;

    private static string server = "192.168.0.13";//"as-dev.rydetec.com";
    private static int port = 49867;

    public static string oculusID;

    public static bool readyForPlayer = false;

    public static void RydeLogin(Message<User> message)
    {
        if (!message.IsError)
        {
            oculusID = message.Data.OculusID;
            OVRDebug.Log("Got Oculus User " + oculusID);
        }
        else
        {
            OVRDebug.Log("Message Error on login!");
        }

        try
        {
            thread = new Thread(new ThreadStart(ListenForData));
            thread.IsBackground = true;
            thread.Start();
        }
        catch (Exception e)
        {
            OVRDebug.Log("Network: " + e);
        }
    }

    // Use this for initialization
    public static void Start()
    {
        OVRDebug.Log("Start network.");
        isStarted = true;
        // Initialize the Platform sdk
        try
        {
            Core.Initialize();
            OVRDebug.Log("Start oculus core.");
        }
        catch (Exception e)
        {
            OVRDebug.Log(e);
        }

        // GetLoggedInuserCallback is some callback function you define
        Users.GetLoggedInUser().OnComplete(RydeLogin);
        OVRDebug.Log("Start oculus user.");


    }

    private static void ListenForData()
    {
        try
        {
            client = new TcpClient(server, port);
            OVRDebug.Log("Client listening.");

            SendCommand(
                new Command(
                    Command.Protocol.hello,
                    new Command.Data[] {
                    new Command.Data("oculusID", "" + oculusID)
                }
                )
            );


            Byte[] bytes = new Byte[1024];
            while (true)
            {
                // Get a stream object for reading              
                using (NetworkStream stream = client.GetStream())
                {
                    int length;
                    // Read incomming stream into byte arrary.                  
                    while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        var incommingData = new byte[length];
                        Array.Copy(bytes, 0, incommingData, 0, length);
                        // Convert byte array to string message.                        
                        string serverMessage = Encoding.ASCII.GetString(incommingData);
                        CommandController.handle(serverMessage);
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            OVRDebug.Log("Network: " + socketException);
        }
    }

    private static void SendString(string message)
    {
        if (client == null)
        {
            return;
        }
        try
        {
            // Get a stream object for writing.             
            NetworkStream stream = client.GetStream();
            if (stream.CanWrite)
            {
                // Convert string message to byte array.                 
                byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(message);
                // Write byte array to socketConnection stream.                 
                stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
            }
        }
        catch (SocketException socketException)
        {
            OVRDebug.Log("Network: " + socketException);
        }
    }

    public static void SendCommand(Command command) {
        //OVRDebug.Log("Sending command " + command.toString());
        SendString(command.toString());
    }

}
