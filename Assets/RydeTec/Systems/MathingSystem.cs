﻿using System;
public class MathingSystem
{
    
    public static float calculateDistance(float[] source, float[] target) {
        return (float) Math.Sqrt(Math.Pow((double) (target[0] - source[0]), 2) + Math.Pow((double) (target[2] - source[2]), 2));
    }

    public static float[] calculateDirection(float[] source, float[] target) {
        float dist = calculateDistance(source, target);
        return new float[] { -((target[0] - source[0]) / dist), -((target[2] - source[2]) / dist) };
    }
}
