using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RydeProtocol
{
    public static string Hello(string username) {
        return "{\"hello\":{\"oculusID\":\"" + username + "\"}}";
    }

    public static string NewCharacter(string name, int slot) {
        return "{\"alter\":{\"new\":{\"character\":\"" + name + "\",\"slot\":\"" + slot + "\"}}}";
    }

    //public static object[] PlayerState(SimpleJSON.JSONNode json) {
    //    if (json["title"] != null) {
            
    //    }
    //}

    public static object[] TitleState(SimpleJSON.JSONNode json) {
        string[] characters = new string[4];
        string[] servers = new string[1];

        if (json != null){
            if (json["title"]!= null) {
                if (json["title"]["characters"] != null)
                {
                    for (int c = 0; c < json["title"]["characters"].Count; c++) {
                        characters[c] = json["title"]["characters"][c];
                    }
                }
                if (json["title"]["servers"] != null)
                {
                    for (int s = 0; s < json["title"]["servers"].Count; s++)
                    {
                        servers[s] = json["title"]["servers"][s];
                    }
                }
                return new object[] {"title", characters, servers };
            }
        }
        return new object[] { };
    }
}
    