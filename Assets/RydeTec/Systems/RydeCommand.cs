using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RydeCommand {

    private static Queue<SimpleJSON.JSONNode> jsonQueue = new Queue<SimpleJSON.JSONNode>();
    private static string data_cache = "";

    public static SimpleJSON.JSONNode parsePartialData( string partial) {
        data_cache += partial;

        string testString = "";
        bool doneWithData = false;
        bool corrupt = false;

        int opens = 0;
        int closes = 0;

        foreach (char c in data_cache)
        {
            if (!doneWithData)
            {
                testString += c;

                if (c == '{') opens++;
                if (c == '}') closes++;

                if (opens == closes)
                {
                    doneWithData = true;
                    data_cache = "";
                }

                if (closes > opens)
                {
                    RydeDebug.Send("Trashing: " + data_cache);
                    doneWithData = true;
                    corrupt = true;
                    data_cache = "";
                }
            }
            else
            {
                data_cache += c;
            }
        }

        if (corrupt)
        {
            return null;
        }

        SimpleJSON.JSONNode command = null;

        try
        {
            command = SimpleJSON.JSON.Parse(testString);
        }
        catch (Exception e)
        {
            RydeDebug.Send("DIDN'T CATCH CORRUPT PROTOCOL! " + testString + " " + e.ToString());
        }

        return command;
    }


    public static void handle(string data) {
        RydeDebug.Send(data);

        SimpleJSON.JSONNode node = null;

        try {
            node = parsePartialData(data);
        } catch (Exception e) {
            RydeDebug.Send("Couldn't parse partial data " + e.ToString());
            return;
        }

        if (node == null) {
            RydeDebug.Send("Unparsable partial data");
            return;
        }

        jsonQueue.Enqueue(node);
    }

    public static SimpleJSON.JSONNode nextCommand() {
        if (jsonQueue.Count < 1) {
            return null;
        }
        return jsonQueue.Dequeue();
    }
}