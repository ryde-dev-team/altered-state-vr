using UnityEngine;
using System.Collections;

public class NewMonoBehaviour : MonoBehaviour
{
    // Use this for initialization

    static GameObject controlObject;
    static GameObject centerEye;

    static GameObject inventoryScreen;
    static GameObject debugLog;
    static GameObject characterScreen;
    static GameObject skillScreen;

    static GameObject bankScreen;

    public static bool inventoryActive;

    bool cleared;


	void Start()
	{
        OVRDebug.Log("Debug log started");

        controlObject = GameObject.Find("Control");

        centerEye = GameObject.Find("CenterEyeAnchor");

        inventoryScreen = GameObject.Find("InventoryScreen");
        debugLog = GameObject.Find("DebugLog");
        characterScreen = GameObject.Find("CharacterScreen");
        skillScreen = GameObject.Find("SkillScreen");

        bankScreen = GameObject.Find("BankScreen");

        cleared = false;
        inventoryActive = false;

	}



    // Update is called once per frame
    void Update()
    {

        controlObject.transform.position = centerEye.transform.position;

        if (!cleared) {
            if (inventoryScreen.activeInHierarchy)
            {
                inventoryScreen.SetActive(false);
            }
            if (debugLog.activeInHierarchy)
            {
                debugLog.SetActive(true);
            }
            if (characterScreen.activeInHierarchy)
            {
                characterScreen.SetActive(false);
            }
            if (skillScreen.activeInHierarchy)
            {
                skillScreen.SetActive(false);
            }
            if (bankScreen.activeInHierarchy)
            {
                bankScreen.SetActive(false);
                BankController.bankActive = false;
                StoreController.modeActive = false;
            }

            cleared = true;
        }

        if (debugLog.activeInHierarchy) { 
            TextMesh textObject = GameObject.Find("DebugText0").GetComponent<TextMesh>();
            if (OVRDebug.text != "")
            {
                textObject.text = OVRDebug.text;
            }
        }



        if (skillScreen.activeInHierarchy)
        {
            TextMesh skillText = GameObject.Find("SkillText").GetComponent<TextMesh>();
            skillText.text = "";

            for (int i = 0; i < RydePlayerController.skills.Length; i++) {
                skillText.text += RydePlayerController.skills[i].name + " " + RydePlayerController.skills[i].current + "/99 " +
                    RydePlayerController.skills[i].currentXP + "/" + RydePlayerController.skills[i].nextLevelXP +
                    "\n";
            }

        }




        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            OVRDebug.Log("One button: " + OVRInput.GetDown(OVRInput.Button.One)); 
        }

        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            OVRDebug.Log("Two button: " + OVRInput.GetDown(OVRInput.Button.Two));
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OVRDebug.Log("Back button: " + Input.GetKey(KeyCode.Escape));
        }

        if (!BankController.bankActive && !StoreController.modeActive) {
            if (OVRInput.GetUp(OVRInput.Button.One))
            {
                //GameObject.Find("DebugLog").SetActive(!GameObject.Find("DebugLog").activeInHierarchy);
                if (inventoryScreen.activeInHierarchy)
                {
                    inventoryScreen.SetActive(false);
                }
                if (debugLog.activeInHierarchy)
                {
                    debugLog.SetActive(false);
                }
                if (characterScreen.activeInHierarchy)
                {
                    characterScreen.SetActive(false);
                }
                if (skillScreen.activeInHierarchy)
                {
                    skillScreen.SetActive(false);
                }
                if (bankScreen.activeInHierarchy)
                {
                    bankScreen.SetActive(false);
                    BankController.bankActive = false;
                    StoreController.modeActive = false;

                }

                inventoryActive = false;

            }

            if (OVRInput.GetDown(OVRInput.Button.One))
            {
                //GameObject.Find("CenterEyeAnchor").GetComponents<Mesh>

                controlObject.transform.rotation = centerEye.transform.rotation;

                if (!inventoryScreen.activeInHierarchy)
                {
                    inventoryScreen.SetActive(true);
                }
                if (!debugLog.activeInHierarchy)
                {
                    debugLog.SetActive(true);
                }
                if (!characterScreen.activeInHierarchy)
                {
                    characterScreen.SetActive(true);
                }
                if (!skillScreen.activeInHierarchy)
                {
                    skillScreen.SetActive(true);
                }

                if (bankScreen.activeInHierarchy)
                {
                    bankScreen.SetActive(false);
                    BankController.bankActive = false;
                    StoreController.modeActive = false;
                }

                inventoryActive = true;

                RydePlayerController.moving = false;
                GameObject.Find("Footsteps").GetComponent<AudioSource>().Stop();
            }
        } else {
            if (OVRInput.GetDown(OVRInput.Button.Down)) {
                if (inventoryScreen.activeInHierarchy)
                {
                    inventoryScreen.SetActive(false);
                }
                if (debugLog.activeInHierarchy)
                {
                    debugLog.SetActive(false);
                }
                if (characterScreen.activeInHierarchy)
                {
                    characterScreen.SetActive(false);
                }
                if (skillScreen.activeInHierarchy)
                {
                    skillScreen.SetActive(false);
                }
                if (bankScreen.activeInHierarchy)
                {
                    bankScreen.SetActive(false);
                    BankController.bankActive = false;
                    StoreController.modeActive = false;

                }
            }
        }



	}
}
